<?php

namespace GildedRose\Test\Functional;

use GildedRose\GildedRose;
use GildedRose\Item\Item;
use GildedRose\Item\ItemValidator;
use GildedRose\UpdatingStrategy\UpdatingStrategyResolver;

class GildedRoseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Экземпляр приложения
     *
     * @var GildedRose
     */
    protected $gildedRose;

    protected function setUp()
    {
        $config = include(__DIR__ . '/../../config.php');
        $this->gildedRose = new GildedRose(
            new UpdatingStrategyResolver($config),
            new ItemValidator()
        );
    }

    function test_AllItemTypesScenario()
    {
        /** @var Item[] $items */
        $items = [
            new Item('+5 Dexterity Vest', 10, 20),
            new Item('Aged Brie', 2, 0),
            new Item('Elixir of the Mongoose', 5, 7),
            new Item('Sulfuras, Hand of Ragnaros', 0, 80),
            new Item('Sulfuras, Hand of Ragnaros', -1, 80),
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
            new Item('Conjured Mana Cake', 3, 6)
        ];
        $this->gildedRose->setItems($items);

        $this->gildedRose->updateQuality();

        $this->assertEquals(19, $items[0]->quality);
        $this->assertEquals(1, $items[1]->quality);
        $this->assertEquals(6, $items[2]->quality);
        $this->assertEquals(80, $items[3]->quality);
        $this->assertEquals(80, $items[4]->quality);
        $this->assertEquals(21, $items[5]->quality);
        $this->assertEquals(50, $items[6]->quality);
        $this->assertEquals(50, $items[7]->quality);
        $this->assertEquals(4, $items[8]->quality);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    function test_InvalidItemScenario()
    {
        /** @var Item[] $items */
        $items = [
            new Item('+5 Dexterity Vest', 10, 20),
            new Item('Aged Brie', 2, -1), // Invalid quality value
        ];
        $this->gildedRose->setItems($items);

        $this->gildedRose->updateQuality();
    }
} 