<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\UpdatingStrategy\ConjuredStrategy;

class ConjuredStrategyTest extends AbstractStrategyTest
{
    protected function setUp()
    {
        $this->strategy = new ConjuredStrategy();
    }

    function test_SaleInDecreases()
    {
        $item = $this->buildItem(5, 10);

        $this->strategy->update($item);

        $this->assertEquals(4, $item->sell_in);
    }

    function test_WhenSellInStaysNonNegative_QualityDecreases()
    {
        $item1 = $this->buildItem(5, 10);
        $item2 = $this->buildItem(1, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(8, $item1->quality);
        $this->assertEquals(8, $item2->quality);
    }

    function test_WhenSellInBecomesNegative_QualityDecreasesTwiceAsFast()
    {
        $item1 = $this->buildItem(0, 10);
        $item2 = $this->buildItem(-3, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(6, $item1->quality);
        $this->assertEquals(6, $item2->quality);
    }

    function test_QualityIsNonNegative()
    {
        $item1 = $this->buildItem(1, 0);  // Regular decreasing
        $item2 = $this->buildItem(-1, 1); // Twice as fast decreasing

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(0, $item1->quality);
        $this->assertEquals(0, $item2->quality);
    }
} 