<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\UpdatingStrategy as Strategy;
use GildedRose\UpdatingStrategy\UpdatingStrategyResolver;

class UpdatingStrategyResolverTest extends \PHPUnit_Framework_TestCase
{
    public function test_WhenNoConfigurationGiven_ReturnsDefaultStrategy()
    {
        $resolver = new UpdatingStrategyResolver();
        $item = new Item('test', 1, 10);

        $strategy = $resolver->getStrategyForItem($item);

        $this->assertInstanceOf(Strategy\DefaultStrategy::class, $strategy);
    }

    public function test_WhenConfigurationGiven_ReturnsConfiguredStrategy()
    {
        $config = [
            'updating_strategies' => [
                'test1' => Strategy\AgedBrieStrategy::class,
                'test2' => Strategy\SulfurasStrategy::class,
            ]
        ];
        $resolver = new UpdatingStrategyResolver($config);
        $item1 = new Item('test1', 1, 10);
        $item2 = new Item('test2', 1, 10);

        $strategy1 = $resolver->getStrategyForItem($item1);
        $strategy2 = $resolver->getStrategyForItem($item2);

        $this->assertInstanceOf(Strategy\AgedBrieStrategy::class, $strategy1);
        $this->assertInstanceOf(Strategy\SulfurasStrategy::class, $strategy2);
    }

    public function test_WhenConfigurationGiven_ForNonConfiguredItem_ReturnsDefaultStrategy()
    {
        $config = [
            'updating_strategies' => [
                'test1' => Strategy\AgedBrieStrategy::class,
                'test2' => Strategy\SulfurasStrategy::class,
            ]
        ];
        $resolver = new UpdatingStrategyResolver($config);
        $item = new Item('test3', 1, 10);

        $strategy = $resolver->getStrategyForItem($item);

        $this->assertInstanceOf(Strategy\DefaultStrategy::class, $strategy);
    }
} 