<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\UpdatingStrategy\UpdatingStrategyInterface;

/**
 * Абстрактный класс теста для проверки стратегий
 */
abstract class AbstractStrategyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Стратегия для проверки
     *
     * @var UpdatingStrategyInterface
     */
    protected $strategy;

    /**
     * Создает экземпляр товара
     *
     * @param $saleIn
     * @param $quality
     * @return Item
     */
    protected function buildItem($saleIn, $quality)
    {
        return new Item('test', $saleIn, $quality);
    }

}
