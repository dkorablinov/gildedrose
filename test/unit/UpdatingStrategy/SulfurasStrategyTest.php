<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\UpdatingStrategy\SulfurasStrategy;

class SulfurasStrategyTest extends AbstractStrategyTest
{
    protected function setUp()
    {
        $this->strategy = new SulfurasStrategy();
    }

    function test_ItemNotChanged()
    {
        $item = $this->buildItem(5, 10);

        $this->strategy->update($item);

        $this->assertEquals(5, $item->sell_in);
        $this->assertEquals(10, $item->quality);
    }
} 