<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\Item\Quality;
use GildedRose\UpdatingStrategy\AgedBrieStrategy;

class AgedBrieStrategyTest extends AbstractStrategyTest
{
    protected function setUp()
    {
        $this->strategy = new AgedBrieStrategy();
    }

    function test_SaleInDecreases()
    {
        $item = $this->buildItem(5, 10);

        $this->strategy->update($item);

        $this->assertEquals(4, $item->sell_in);
    }

    function test_WhenQualityIsLessThanMax_IncreasesQuality()
    {
        $item = $this->buildItem(5, 10);

        $this->strategy->update($item);

        $this->assertEquals(11, $item->quality);
    }

    function test_WhenQualityIsMaximum_QualityNotChanged()
    {
        $item = $this->buildItem(5, Quality::UPPER_LIMIT);

        $this->strategy->update($item);

        $this->assertEquals(Quality::UPPER_LIMIT, $item->quality);
    }
} 