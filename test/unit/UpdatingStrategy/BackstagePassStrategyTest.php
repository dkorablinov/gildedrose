<?php

namespace GildedRose\Test\Unit\UpdatingStrategy;

use GildedRose\Item\Quality;
use GildedRose\UpdatingStrategy\BackstagePassStrategy;

class BackstagePassStrategyTest extends AbstractStrategyTest
{
    protected function setUp()
    {
        $this->strategy = new BackstagePassStrategy();
    }

    function test_SaleInDecreases()
    {
        $item = $this->buildItem(5, 10);

        $this->strategy->update($item);

        $this->assertEquals(4, $item->sell_in);
    }

    function test_WhenSellInStaysGreaterOrEqualTo10_IncreasesQualityBy1()
    {
        $item1 = $this->buildItem(15, 10);
        $item2 = $this->buildItem(11, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(11, $item1->quality);
        $this->assertEquals(11, $item2->quality);
    }

    function test_WhenSellInStaysGreaterOrEqualTo5_IncreasesQualityBy2()
    {
        $item1 = $this->buildItem(10, 10);
        $item2 = $this->buildItem(6, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(12, $item1->quality);
        $this->assertEquals(12, $item2->quality);
    }

    function test_WhenSellInStaysGreaterOrEqualTo0_IncreasesQualityBy3()
    {
        $item1 = $this->buildItem(5, 10);
        $item2 = $this->buildItem(1, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(13, $item1->quality);
        $this->assertEquals(13, $item2->quality);
    }

    function test_WhenSellInBecomesNegative_DropsQualityTo0()
    {
        $item1 = $this->buildItem(0, 10);
        $item2 = $this->buildItem(-2, 10);

        $this->strategy->update($item1);
        $this->strategy->update($item2);

        $this->assertEquals(0, $item1->quality);
        $this->assertEquals(0, $item2->quality);
    }

    function test_WhenQualityIsMaximum_QualityNotChanged()
    {
        $item1 = $this->buildItem(11, Quality::UPPER_LIMIT); // Regularly increases by 1
        $item2 = $this->buildItem(6, Quality::UPPER_LIMIT);  // Regularly increases by 2
        $item3 = $this->buildItem(1, Quality::UPPER_LIMIT);  // Regularly increases by 3

        $this->strategy->update($item1);
        $this->strategy->update($item2);
        $this->strategy->update($item3);

        $this->assertEquals(Quality::UPPER_LIMIT, $item1->quality);
        $this->assertEquals(Quality::UPPER_LIMIT, $item2->quality);
        $this->assertEquals(Quality::UPPER_LIMIT, $item3->quality);
    }
} 