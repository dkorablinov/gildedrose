<?php

return [
    'updating_strategies' => [
        'Sulfuras, Hand of Ragnaros' => \GildedRose\UpdatingStrategy\SulfurasStrategy::class,
        'Aged Brie' => \GildedRose\UpdatingStrategy\AgedBrieStrategy::class,
        'Backstage passes to a TAFKAL80ETC concert' => \GildedRose\UpdatingStrategy\BackstagePassStrategy::class,
        'Conjured Mana Cake' => \GildedRose\UpdatingStrategy\ConjuredStrategy::class,
    ]
];
 