<?php

namespace GildedRose\Item;

/**
 * Трейт с методами для обновления параметров товара
 */
trait ItemUpdatingTrait
{

    /**
     * Уменьшает срок реализации
     *
     * @param Item $item
     * @param int $decrement
     */
    protected function decreaseSellIn(Item $item, $decrement = 1)
    {
        $item->sell_in -= $decrement;
    }

    /**
     * Повышает качество
     *
     * @param Item $item
     * @param int $increment
     */
    protected function increaseQuality(Item $item, $increment = Quality::DEFAULT_INCREMENT)
    {
        $item->quality = min($item->quality + $increment, Quality::UPPER_LIMIT);
    }

    /**
     * Понижает качество
     *
     * @param Item $item
     * @param int $decrement
     */
    protected function decreaseQuality(Item $item, $decrement = Quality::DEFAULT_DECREMENT)
    {
        $item->quality = max($item->quality - $decrement, Quality::LOWER_LIMIT);
    }
} 