<?php

namespace GildedRose\Item;

/**
 * Качество товара
 *
 * Class Quality
 * @package GildedRose\Item
 */
class Quality 
{
    /** Нижний порог значения */
    const LOWER_LIMIT = 0;

    /** Верхний порог значения */
    const UPPER_LIMIT = 50;

    /** Приращение значения по умолчанию */
    const DEFAULT_INCREMENT = 1;

    /** Уменьшение значения по умолчанию */
    const DEFAULT_DECREMENT = 1;
} 