<?php

namespace GildedRose\Item;

/**
 * Валидатор товара
 *
 * Class ItemValidator
 * @package GildedRose\Item
 */
class ItemValidator 
{
    /**
     * Проверяет валидность товара
     *
     * @param Item $item
     * @return bool
     */
    public function isValid(Item $item)
    {
        return
            $this->qualityIsValid($item)
            && $this->sellInIsValid($item)
            && $this->nameIsValid($item);
    }

    /**
     * @param $item
     * @return bool
     */
    private function qualityIsValid($item)
    {
        return is_integer($item->quality) && ($item->quality >= 0);
    }

    /**
     * @param $item
     * @return bool
     */
    private function sellInIsValid($item)
    {
        return is_integer($item->sell_in);
    }

    /**
     * @param $item
     * @return bool
     */
    private function nameIsValid($item)
    {
        return is_string($item->name) && !empty($item->name);
    }
} 