<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\Item\ItemUpdatingTrait;
use GildedRose\Item\Quality;

/**
 * Стратегия для товара Backstage Pass
 *
 * Class BackstagePassStrategy
 * @package GildedRose\UpdatingStrategy
 */
class BackstagePassStrategy implements UpdatingStrategyInterface
{
    use ItemUpdatingTrait;

    /** Верхний порог значения срока реализации для максимального увеличения качества */
    const SELL_IN_UPPER_LIMIT_FOR_MAX_INCREMENT = 5;

    /** Верхний порог значения срока реализации для среднего увеличения качества */
    const SELL_IN_UPPER_LIMIT_FOR_MID_INCREMENT = 10;

    /**
     * Изменяет качество товара
     *
     * @param Item $item
     * @return void
     */
    public function update(Item $item)
    {
        $this->decreaseSellIn($item);

        if ($item->sell_in < 0) {
            $item->quality = 0;

            return;
        }

        $qualityIncrement =
            ($item->sell_in < self::SELL_IN_UPPER_LIMIT_FOR_MAX_INCREMENT)
                ? 3
                : (
                    ($item->sell_in < self::SELL_IN_UPPER_LIMIT_FOR_MID_INCREMENT)
                        ? 2
                        : Quality::DEFAULT_INCREMENT
                );
        $this->increaseQuality($item, $qualityIncrement);
    }
}