<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;

/**
 * Интерфейс стратегии изменения параметров товара
 */
interface UpdatingStrategyInterface
{
    /**
     * Изменяет параметры товара
     *
     * @param \GildedRose\Item\Item $item
     * @return void
     */
    public function update(Item $item);
} 