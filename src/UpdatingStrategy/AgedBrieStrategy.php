<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\Item\ItemUpdatingTrait;

/**
 * Стратегия для товара Aged Brie
 *
 * Class AgedBrieStrategy
 * @package GildedRose\UpdatingStrategy
 */
class AgedBrieStrategy implements UpdatingStrategyInterface
{
    use ItemUpdatingTrait;

    /**
     * Изменяет качество товара
     *
     * @param Item $item
     * @return void
     */
    public function update(Item $item)
    {
        $this->decreaseSellIn($item);
        $this->increaseQuality($item);
    }
}