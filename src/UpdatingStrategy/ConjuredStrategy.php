<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\Item\ItemUpdatingTrait;
use GildedRose\Item\Quality;

/**
 * Стратегия для товара Conjured
 *
 * Class ConjuredStrategy
 * @package GildedRose\UpdatingStrategy
 */
class ConjuredStrategy implements UpdatingStrategyInterface
{
    use ItemUpdatingTrait;

    /**
     * Изменяет параметры товара
     *
     * @param \GildedRose\Item\Item $item
     * @return void
     */
    public function update(Item $item)
    {
        $this->decreaseSellIn($item);

        $qualityDecrement =
            ($item->sell_in < 0)
                ? Quality::DEFAULT_DECREMENT * 4
                : Quality::DEFAULT_DECREMENT * 2;
        $this->decreaseQuality($item, $qualityDecrement);
    }
}