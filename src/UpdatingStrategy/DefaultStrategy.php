<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;
use GildedRose\Item\ItemUpdatingTrait;
use GildedRose\Item\Quality;

/**
 * Стратегия по умолчанию для изменения параметров товара
 *
 * Class DefaultStrategy
 * @package GildedRose\UpdatingStrategy
 */
class DefaultStrategy implements UpdatingStrategyInterface
{
    use ItemUpdatingTrait;

    /**
     * Изменяет качество товара
     *
     * @param Item $item
     * @return void
     */
    public function update(Item $item)
    {
        $this->decreaseSellIn($item);

        $qualityDecrement =
            ($item->sell_in < 0)
                ? Quality::DEFAULT_DECREMENT * 2
                : Quality::DEFAULT_DECREMENT;
        $this->decreaseQuality($item, $qualityDecrement);
    }
}