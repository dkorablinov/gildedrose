<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;

/**
 * Механизм определения нужной стратегии обновления параметров товара
 *
 * Class UpdatingStrategyResolver
 * @package GildedRose
 */
class UpdatingStrategyResolver
{
    /** Название ключа в конфигурации приложения, который содержит перечень стратегий обновления параметров товаров */
    const UPDATING_STRATEGIES_CONFIG_KEY = 'updating_strategies';

    /**
     * Реестр стратегий обновления параметров товаров
     *
     * @var UpdatingStrategyInterface[]
     */
    private $strategyRegistry = [];

    /**
     * Стратегия по-умолчанию
     *
     * @var UpdatingStrategyInterface
     */
    private $defaultStrategy;

    /**
     * @param array $config
     * @throws \RuntimeException
     */
    public function __construct(array $config = [])
    {
        $this->defaultStrategy = new DefaultStrategy();

        if (isset($config[self::UPDATING_STRATEGIES_CONFIG_KEY])) {
            $updatingStrategiesConfig = $config[self::UPDATING_STRATEGIES_CONFIG_KEY];

            if (is_array($updatingStrategiesConfig) && !empty($updatingStrategiesConfig)) {
                foreach ($updatingStrategiesConfig as $itemName => $strategyClassName) {
                    if (! class_exists($strategyClassName)) {
                        throw new \RuntimeException(
                            sprintf('Configuration for updating strategies failed: class %s is undefined', $strategyClassName)
                        );
                    }

                    $this->strategyRegistry[$itemName] = new $strategyClassName();
                }
            }
        }
    }

    /**
     * Возвращает стратегию для указанного товара
     *
     * @param Item $item
     * @return UpdatingStrategyInterface
     */
    public function getStrategyForItem(Item $item)
    {
        if (array_key_exists($item->name, $this->strategyRegistry)) {
            return $this->strategyRegistry[$item->name];
        }

        return $this->defaultStrategy;
    }
} 