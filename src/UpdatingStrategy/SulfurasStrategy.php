<?php

namespace GildedRose\UpdatingStrategy;

use GildedRose\Item\Item;

/**
 * Стратегия для товара Sulfuras
 *
 * Class SulfurasStrategy
 * @package GildedRose\UpdatingStrategy
 */
class SulfurasStrategy implements UpdatingStrategyInterface
{

    /**
     * Изменяет качество товара
     *
     * @param \GildedRose\Item\Item $item
     * @return void
     */
    public function update(Item $item)
    {
        // Ничего не делаем: качество и срок реализации данного вида товаров неизменны
        return;
    }
}