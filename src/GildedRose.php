<?php

namespace GildedRose;
use GildedRose\Item\Item;
use GildedRose\Item\ItemValidator;
use GildedRose\UpdatingStrategy\UpdatingStrategyResolver;

/**
 * Приложение инвентаризации
 *
 * Class GildedRose
 * @package GildedRose
 */
class GildedRose
{
    /**
     * Перечень товаров
     *
     * @var Item[]
     */
    private $items;

    /**
     * Механизм выбора стратегии обновления качества товара
     *
     * @var UpdatingStrategyResolver
     */
    private $updatingStrategyResolver;

    /**
     * Валидатор товаров
     *
     * @var ItemValidator
     */
    private $itemValidator;

    /**
     * @param UpdatingStrategyResolver $resolver
     * @param \GildedRose\Item\ItemValidator $itemValidator
     */
    public function __construct(UpdatingStrategyResolver $resolver, ItemValidator $itemValidator)
    {
        $this->updatingStrategyResolver = $resolver;
        $this->itemValidator = $itemValidator;
    }

    /**
     * Задает перечень товаров
     *
     * @param array $items
     * @throws \InvalidArgumentException
     */
    public function setItems(array $items)
    {
        $this->items = [];

        foreach ($items as $item) {
            if (! $item instanceof Item) {
                throw new \InvalidArgumentException('Inventory item must be an instance of GildedRose\Item\Item');
            }

            $this->addItem($item);
        }
    }

    /**
     * @param Item $item
     * @throws \InvalidArgumentException
     */
    private function addItem(Item $item)
    {
        if (! $this->itemValidator->isValid($item)) {
            throw new \InvalidArgumentException('Item not valid: ' . $item);
        }

        $this->items[] = $item;
    }

    /**
     * Обновляет параметры всех товаров в перечене
     */
    public function updateQuality()
    {
        if (! empty($this->items)) {
            foreach ($this->items as $item) {
                $updatingStrategy = $this->updatingStrategyResolver->getStrategyForItem($item);
                $updatingStrategy->update($item);
            }
        }
    }
}


 